# Background

Onnistuu.fi is a digital signature service, you need a contract with them to use the service. Signing a document - it has
to be a PDF - means redirecting the user from Drupal to https://www.onnistuu.fi/, where the user is asked for banking codes. After 
  successful authentication a digital signature page is added to the PDF, and the user is redirected to the Drupal site.

Note that the module doesn't do much by itself, you need to take care of constructing a valid signing URL that will 
launch the signing wizard of two steps. E.g. http://example.com/onnistuu/sign?document=http://example.com/path/to/example.pdf

IMPORTANT: there is no guarantee that the full process is secure, unless you take care of your side of the code. E.g. when
you construct the URL you could hash the file name with a good hash and a good salt.

# Configuration

Enable the module and configure it at admin/config/services/onnistuu

# Requirements

PHP's mcrypt needs to be installed and configured on the server.

# Notes

* Tested with PHP 5.3 on a linux host.
* Written to use version 0.6 of the Onnistuu.fi API.
* Tested API version is OnnistuuAPI-0.6-Client-0.3

# Usage example code

```
// include library / use autoloader
// the following keys are examples
$client = new OnnistuuExternalClient(
  'ede412ec-5a86-4cf5-8f9d-25e6b6dfae9d',
  'sE1RdFPXZXaDrIpZ3XiFYdEQlF63Yt9GaEtVFCBQmcU='
);
$encrypted = $client->encryptRequest(array(
    'stamp' => '123456789',
    'return_success' => 'https://your.address.tld/onnistuu-return-path',
    'document' => 'https://your.address.tld/document-path-123456789',
    'requirements' => array(
        array(
            'type' => 'person',
            // Nordea test identifier, won't work on the live service
            'identifier' => '010100-123D',
        ),
        array(
            'type' => 'email',
            'identifier' => '010100-123D',
            'email' => 'address.to@invite.tld',
            'sms' => '+358401234567', // optional
        ),
    ),
    // 'afterwards_invite_email' => 'you@your.address.tld', -- DEPRECATED
    // 'document_check_url => 'https://your.address.tld/check-document-path',
));
// create POST form with $encrypted['data'], $encrypted['iv'], customer id,
// return_failure url and optional auth_service selection
```
