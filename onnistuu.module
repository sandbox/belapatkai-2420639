<?php

/**
 * Implements hook_menu().
 */
function onnistuu_menu() {
  $items['admin/config/services/onnistuu'] = array(
    'title' => 'Onnistuu.fi configuration',
    'description' => 'Onnistuu.fi API configuration',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('onnistuu_admin_settings'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'includes/onnistuu.admin.inc',
  );

  $items['onnistuu/success'] = array(
    'title' => 'Success',
    'page callback' => 'onnistuu_return_success',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );

  $items['onnistuu/error'] = array(
    'title' => 'Error',
    'page callback' => 'onnistuu_return_error',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );

  $items['onnistuu/sign'] = array(
    'title' => 'Sign the document',
    'page callback' => 'onnistuu_sign',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Defines steps.
 */
function _onnistuu_steps() {
  return array(
    1 => array(
      'form' => 'onnistuu_sign_form_step1',
    ),
    2 => array(
      'form' => 'onnistuu_sign_form_step2',
    ),
  );
}

/**
 * Get sign form.
 */
function onnistuu_sign() {
  return drupal_get_form('onnistuu_sign_form');
}

/**
 * Form constructor for the sign form.
 */
function onnistuu_sign_form($form, &$form_state) {

  // Initialize a description of the steps for the wizard.
  if (empty($form_state['step'])) {
    $form_state['step'] = 1;

    // This array contains the function to be called at each step to get the
    // relevant form elements. It will also store state information for each
    // step.
    $form_state['step_information'] = _onnistuu_steps();
  }

  $step = &$form_state['step'];
  drupal_set_title(t('Sign the document: Step @step', array('@step' => $step)));
  $function = $form_state['step_information'][$step]['form'];

  $form = $function($form, $form_state);

  // Show the Next button only if there are more steps defined.
  if ($step < count($form_state['step_information']) && !empty($form)) {
    // The Next button should be included on every step.
    $form['next'] = array(
      '#type' => 'submit',
      '#value' => t('Next'),
      '#name' => 'next',
      '#submit' => array('onnistuu_sign_form_next_submit'),
    );
  }
  elseif (!empty($form)) {
    // Just in case there are no more steps, we use the default submit handler
    // of the form wizard. Call this button Finish, Submit, or whatever you
    // want to show. When this button is clicked, the
    // form_example_wizard_submit handler will be called.
    $form['#action'] = url('https://www.onnistuu.fi/external/entry/');
    $form['finish'] = array(
      '#type' => 'submit',
      '#value' => t('Sign'),
    );
  }

  // Include each validation function defined for the different steps.
  if (function_exists($form_state['step_information'][$step]['form'] . '_validate')) {
    $form['next']['#validate'] = array($form_state['step_information'][$step]['form'] . '_validate');
  }

  return $form;
}

/**
 * Submit handler for the 'next' button.
 *
 * This function:
 * - Saves $form_state['values']
 * - Increments the step count.
 * - Replace $form_state['values'] from the last time we were at this page
 *   or with array() if we haven't been here before.
 * - Force form rebuild.
 */
function onnistuu_sign_form_next_submit($form, &$form_state) {
  $current_step = &$form_state['step'];
  $form_state['step_information'][$current_step]['stored_values'] = $form_state['values'];

  if ($current_step < count($form_state['step_information'])) {
    if (!empty($form_state['step_information'][$current_step]['stored_values'])) {
      $form_state['values'] = $form_state['step_information'][$current_step]['stored_values'];
    }
    else {
      $form_state['values'] = array();
    }
    $current_step++;

    // Force rebuild with next step.
    $form_state['rebuild'] = TRUE;
    return;
  }
}

/**
 * Implementation of step1
 */
function onnistuu_sign_form_step1($form, &$form_state) {

  if (empty($_GET['document'])) {
    drupal_set_message(t('You need to define document for signing.'), 'error');
    return;
  }

  $signing_options = variable_get('onnistuu_signing_options', '');

  if ($signing_options['Personal ID']) {
    $form['personal_id'] = array(
      '#title' => 'Personal ID',
      '#type' => 'textfield',
      '#default_value' => '',
      '#description' => t('Give your Personal ID for signing.'),
    );
  }

  if ($signing_options['Company ID']) {
    $form['company_id'] = array(
      '#title' => 'Company ID',
      '#type' => 'textfield',
      '#default_value' => '',
      '#description' => t('Give Company ID for signing.'),
    );
  }

  if ($signing_options['Email invitation']) {
    $form['email_invitation'] = array(
      '#title' => 'Email invitation',
      '#type' => 'textarea',
      '#default_value' => '',
      '#description' => t('List of emails for signing. Personal ID|email address on one line. Example: 1234567-123E|address.to@invite.com'),
    );
  }

  $doc = _onnistuu_check_document_uri($_GET['document']);

  $form['document'] = array(
    '#type' => 'hidden',
    '#value' => $doc,
  );

  if (!empty($_GET['document'])) {
    $form['markup'] = array(
      '#markup' => t('<p>You can check here document for digital signature: <a href="@doc">@doc</a></p>', array('@doc' => $doc)),
    );
  }

  return $form;
}

/**
 * Implementation of step2.
 */
function onnistuu_sign_form_step2($form, &$form_state) {
  global $base_url;

  $requirements = $requirements_emails = array();

  $client = onnistuu_init();

  $stamp = time();

  if (!empty($form_state['values']['personal_id'])) {
    $id = $form_state['values']['personal_id'];
    $requirements = array(
      array(
        'type' => 'person',
        'identifier' => "$id",
      ),
    );
  }

  if (!empty($form_state['values']['company_id'])) {
    $id = $form_state['values']['company_id'];
    $requirements += array(
      array(
        'type' => 'company',
        'identifier' => "$id",
      ),
    );
  }

  if (!empty($form_state['values']['email_invitation'])) {
    $emails = explode("\r\n", $form_state['values']['email_invitation']);
    if (is_array($emails)) {
      foreach ($emails as $row) {
        $row_arr = explode("|", $row);
        $id = $row_arr[0];
        $email = $row_arr[1];
        $requirements_emails[] = array(
          'type' => 'email',
          'identifier' => "$id",
          'email' => $email,
        );
      }
    }
  }

  $requirements = array_merge($requirements, $requirements_emails);

  $doc = $form_state['values']['document'];

  $encrypted = $client->encryptRequest(
    array(
      'stamp' => "$stamp",
      'return_success' => $base_url . '/onnistuu/success',
      'document' => "$doc",
      'requirements' => $requirements,
    )
  );

  $form['markup'] = array(
    '#markup' => t('<p>Please check your information and sign the document.</p><p>Document: <a href="@doc">@doc</a></p>', array('@id' => $id, '@doc' => $doc)),
  );

  $form['return_failure'] = array(
    '#type' => 'hidden',
    '#value' => $base_url . '/onnistuu/error',
  );
  $form['customer'] = array(
    '#type' => 'hidden',
    '#value' => variable_get('onnistuu_customer_id', ''),
  );
  $form['data'] = array(
    '#type' => 'hidden',
    '#value' => $encrypted['data'],
  );
  $form['iv'] = array(
    '#type' => 'hidden',
    '#value' => $encrypted['iv'],
  );

  return $form;
}

/**
 * Implements hook_init.
 */
function onnistuu_init() {
  module_load_include('inc', 'onnistuu', 'includes/client');
  $client = new OnnistuuExternalClient(
    variable_get('onnistuu_customer_id', ''),
    variable_get('onnistuu_crypt_key', '')
  );
  return $client;
}

/**
 * Return if everything went correctly.
 */
function onnistuu_return_success() {
  $data = $_GET['data'];
  $iv = $_GET['iv'];
  $client = onnistuu_init();
  $decrypt_data = $client->decryptReturn($data, $iv);
  $doc = $decrypt_data->document;

  $text = t('Signed document is available here: <a href="@url">@doc</a>', array(
    '@url' => url($doc),
    '@doc' => $doc,
  ));
  return $text;
}

/**
 * Return if fail.
 */
function onnistuu_return_error() {
  $message = $_GET['onnistuu_message'] . '.';
  //$message .= onnistuu_error_handling($_GET['onnistuu_error']);
  $text = t('<p>Error occured while signing. Error message: <br/> @message</p><p>Please contact site administrator.</p>', array('@message' => $message));
  return $text;
}

/**
 * Validates document uri.
 */
function _onnistuu_check_document_uri($uri) {
  // Encode path and put all together manually.
  // TODO: reconsider using http_build_url
  $error = FALSE;
  $parts = parse_url($uri);
  $path = drupal_encode_path($parts['path']);

  if (empty($parts['scheme'])) {
    $error = TRUE;
    $message = t('Missing scheme from document url.');
  }
  else {
    $doc = $parts['scheme'] . '://';
  }
  if (empty($parts['host'])) {
    $error = TRUE;
    $message = t('Missing host from document url.');
  }
  else {
    $doc .= $parts['host'];
  }

  if (!empty($parts['port'])) {
    $doc .= ':' . $parts['port'] . '/';
  }
  else {
    $doc .= '/';
  }

  if (!empty($parts['path'])) {
    $doc .= ltrim($path, '/');
  }

  if (!empty($parts['query'])) {
    $doc .= '?' . $parts['query'];
  }

  if ($error) {
    drupal_set_message($message, 'error');
  }

  return $doc;
}
