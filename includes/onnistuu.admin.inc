<?php

/**
 * The settings form for onnistuu.fi, used typically by site admins
 *
 * @return mixed
 */
function onnistuu_admin_settings() {
  $form['onnistuu_customer_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Onnistuu.fi Customer ID'),
    '#default_value' => variable_get('onnistuu_customer_id', ''),
    '#description' => t('Onnistuu.fi service Customer ID'),
  );

  $form['onnistuu_crypt_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Onnistuu.fi Crypt Key'),
    '#default_value' => variable_get('onnistuu_crypt_key', ''),
    '#description' => t('Onnistuu.fi service crypt key'),
  );

  $form['onnistuu_signing_options'] = array(
    '#type' => 'checkboxes',
    '#options' => drupal_map_assoc(array(
      t('Personal ID'),
      t('Company ID'),
      t('Email invitation'),
    )),
    '#title' => t('Signing options'),
    '#default_value' => variable_get('onnistuu_signing_options', ''),
  );

  return system_settings_form($form);
}
